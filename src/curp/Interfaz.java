package curp;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * En esta clase se implemeta la interfaz de la aplicacion.
 * No tienen que modificar nada de esta clase.
 * @author diego
 */
public class Interfaz extends Application {
    
    @Override
    public void start(Stage stage) {
        GridPane grid = new GridPane();
        Scene scene = new Scene(grid, 300, 550);
        TextField t_nombres = new TextField();
        TextField t_apellido_p = new TextField();
        TextField t_apellido_m = new TextField();
        TextField t_dia = new TextField();
        TextField t_mes = new TextField();
        TextField t_anio = new TextField();
        TextField t_sexo = new TextField();
        TextField t_estado = new TextField();
        Button encontrar = new Button("CURP");
        Label tu_curp = new Label();
        grid.add(new Label("Nombre(s)"), 0, 0);
        grid.add(t_nombres, 0, 1);
        grid.add(new Label("\nApellido Paterno"), 0, 2);
        grid.add(t_apellido_p, 0, 3);
        grid.add(new Label("\nApellido Materno"), 0, 4);
        grid.add(t_apellido_m, 0, 5);
        grid.add(new Label("\nFecha de nacimiento:"), 0, 6);
        grid.add(new Label("Dia"), 0, 7);
        grid.add(t_dia, 0, 8);
        grid.add(new Label("Mes"), 0, 9);
        grid.add(t_mes, 0, 10);
        grid.add(new Label("Año"), 0, 11);
        grid.add(t_anio, 0, 12);
        grid.add(new Label("\nSexo (Mujer/Hombre)"), 0, 13);
        grid.add(t_sexo, 0, 14);
        grid.add(new Label("\nEstado"), 0, 15);
        grid.add(t_estado, 0, 16);
        grid.add(new Label("\n"), 0, 17);
        grid.add(encontrar, 0, 18);
        grid.add(new Label("\n"), 0, 19);
        grid.add(tu_curp, 0, 20);
        grid.setAlignment(Pos.CENTER);
        
        encontrar.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(ActionEvent limpia){
                CURP c = new CURP();
                String curp =  c.apellidoPaterno(t_apellido_p.getText()) + c.apellidoMaterno(t_apellido_m.getText()) + 
                        c.nombre(t_nombres.getText()) + c.fechaNacimiento(t_dia.getText()+ "/" + t_mes.getText() + "/" + t_anio.getText()) + 
                        c.sexo(t_sexo.getText()) + c.estado(t_estado.getText()) + c.getConsonantes(t_apellido_p.getText()) + 
                        c.getConsonantes(t_apellido_m.getText()) +  c.getConsonantes(t_nombres.getText()) + "00";
                tu_curp.setText(curp);
            }
        });
            
        
        stage.setTitle("Encuentra tu CURP");
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}